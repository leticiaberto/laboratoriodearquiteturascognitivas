
import java.util.ArrayList;
import java.util.List;
import ws3dproxy.CommandExecException;
import ws3dproxy.model.Creature;
import ws3dproxy.model.World;
import ws3dproxy.model.WorldPoint;
import ws3dproxy.WS3DProxy;
import ws3dproxy.model.Leaflet;


/**
 *
 * @author ia941
 */
public class Main {
    /**
     * @param args the command line arguments
     * @throws java.lang.InterruptedException
     */
    public static void main(String[] args) throws InterruptedException {
        WS3DProxy proxy = new WS3DProxy();
        try {   
             World w = World.getInstance();
             w.reset();//reset the World if it was already initialized from a previous simulation
             
             //cria comidas no cenario
             World.createFood(0, 350, 75);
             World.createFood(0, 100, 220);
             World.createFood(0, 250, 210);
             
             //cria joias no cenario
             World.createJewel(0, 200, 100);
             World.createJewel(1, 200, 350);
             World.createJewel(2, 300, 100);
             World.createJewel(3, 500, 100);
             World.createJewel(4, 10, 300);
             World.createJewel(5, 600, 400);
             World.createJewel(3, 700, 200);
             World.createJewel(0, 400, 600);
             World.createJewel(2, 100, 100);
             
             Creature c = proxy.createCreature(100,450,0);
             c.genLeaflet();
             c = c.updateState();
             
             JanelaPrincipal jp = new JanelaPrincipal(c);
             jp.setFocusable(true);//para o botão nao "roubar" os eventos de teclado
             jp.setVisible(true);
             //MindWindow mw = new MindWindow(c);
             c.start();//triggers the beginning of the simulation at WS3D
             
             WorldPoint position = c.getPosition();
             double pitch = c.getPitch();
             double fuel = c.getFuel();
             
             while (true){
                c = c.updateState();
                Thread.sleep(3000);
            }
            
        }
        catch (CommandExecException e) {
                System.out.println("Erro capturado"); 
        }
    }
}
