/*
 * Copyright (C) 2018 leticia.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301  USA
 */
package codelets.behaviors;

import br.unicamp.cst.core.entities.Codelet;
import br.unicamp.cst.core.entities.MemoryContainer;
import br.unicamp.cst.core.entities.MemoryObject;
import java.awt.Point;
import java.awt.geom.Point2D;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;
import memory.CreatureInnerSense;
import org.json.JSONException;
import org.json.JSONObject;
import ws3dproxy.model.Thing;

/**
 *
 * @author leticia
 */
public class GetJewel extends Codelet{

    private MemoryObject closestJewelMO;
    private MemoryObject innerSenseMO;
    private MemoryObject knownMO;
    private MemoryObject knownAllJewelsMO;
    private int reachDistance;
    private MemoryContainer handsMO;
    Thing closestJewel;
    CreatureInnerSense cis;
    List<Thing> known;
    List<Thing> knownJewels;
    private int indice = -1;
    private double eval;
    
    public GetJewel(int reachDistance) {
        setTimeStep(50);
        this.reachDistance=reachDistance;
    }

    @Override
    public void accessMemoryObjects() {
        closestJewelMO=(MemoryObject)this.getInput("CLOSEST_LEAFLETJEWEL");
        innerSenseMO=(MemoryObject)this.getInput("INNER");
        handsMO=(MemoryContainer)this.getOutput("HANDS");
        knownMO = (MemoryObject)this.getOutput("KNOWN_JEWELS");
        knownAllJewelsMO = (MemoryObject)this.getInput("KNOWN_JEWELS");
    }

    @Override
    public void calculateActivation() {
    }

    @Override
    public void proc() {
        String jewelName="";
        closestJewel = (Thing) closestJewelMO.getI();
        cis = (CreatureInnerSense) innerSenseMO.getI();
        known = (List<Thing>) knownMO.getI();
        //Find distance between closest apple and self
        //If closer than reachDistance, eat the apple
        //eval = 0.0;
        if(closestJewel != null)
        {
            double jewelX=0;
            double jewelY=0;
            try {
                jewelX=closestJewel.getX1();
                jewelY=closestJewel.getY1();
                jewelName = closestJewel.getName();
            } catch (Exception e) {
                    // TODO Auto-generated catch block
                e.printStackTrace();
            }

            double selfX=cis.position.getX();
            double selfY=cis.position.getY();

            Point2D pJewel = new Point();
            pJewel.setLocation(jewelX, jewelY);

            Point2D pSelf = new Point();
            pSelf.setLocation(selfX, selfY);

            double distance = pSelf.distance(pJewel);
            JSONObject message=new JSONObject();
            
            if(cis.fuel >= 400)
                eval = 1.0;
            else
                eval = 0.0;
            
            try {
                if(distance<reachDistance){ //eat it						
                    message.put("OBJECT", jewelName);
                    message.put("ACTION", "PICKUP");
                    if(indice == -1)
                        indice = handsMO.setI(message.toString());
                    else
                        handsMO.setI(message.toString(), eval, indice);
                    DestroyClosestJewel();
                }else{
                    
                    knownJewels = Collections.synchronizedList((List<Thing>) knownAllJewelsMO.getI());
                    boolean flag = true;
                    
                    synchronized(knownJewels) {
                        if(knownJewels.size() != 0){
                             //Iterate over objects in vision, looking for the closest apple
                            CopyOnWriteArrayList<Thing> myknown = new CopyOnWriteArrayList<>(knownJewels);
                            for (Thing t : myknown) {
                                String objectName=t.getName();
                                //if(objectName.contains("Jewel")){ //Then, it is an jewel
                                    double Dnew = calculateDistance(t.getX1(), t.getY1(), cis.position.getX(), cis.position.getY());//distancia da joia sendo vista
                                    if (Dnew < reachDistance){
                                        flag = false;
                                        eval = 1.2;
                                        //System.out.println("name: " + t.getName());
                                        message.put("OBJECT", t.getName());
                                        message.put("ACTION", "PICKUP");
                                        if(indice == -1)
                                            indice = handsMO.setI(message.toString());
                                        else
                                            handsMO.setI(message.toString(), eval, indice);
                                        DestroyClosestJewel(t);
                                    }
                                //}
                            }      
                        }
                    }
                    
                    if(flag){
                        if(indice == -1)
                            indice = handsMO.setI("");
                        else
                            handsMO.setI("", eval, indice);//nothing
                    }
                    //handsMO.setI("");	//nothing
                }

    //				System.out.println(message);
            } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
            }
        }else{
            if(indice == -1)
                indice = handsMO.setI("");
            else
                handsMO.setI("", eval, indice);//nothing
               // handsMO.setI("");	//nothing
        }
    }
    
    public void DestroyClosestJewel() {
        int r = -1;
        int i = 0;
        synchronized(known) {
          CopyOnWriteArrayList<Thing> myknown = new CopyOnWriteArrayList<>(known);  
          for (Thing t : known) {
           if (closestJewel != null) 
              if (t.getName().equals(closestJewel.getName())) r = i;
           i++;
          }   
          if (r != -1) known.remove(r);
          closestJewel = null;
        }
    }
    
    public void DestroyClosestJewel(Thing jewel) {
        int r = -1;
        int i = 0;
        synchronized(knownJewels) {
          CopyOnWriteArrayList<Thing> myknown = new CopyOnWriteArrayList<>(knownJewels);  
          for (Thing t : knownJewels) {
           //if (jewel != null) 
              if (t.getName().equals(jewel.getName())) r = i;
           i++;
          }   
          if (r != -1) knownJewels.remove(r);
          //closestJewel = null;
        }
    }
    
    private double calculateDistance(double x1, double y1, double x2, double y2) {
            return(Math.sqrt(Math.pow(x1-x2, 2)+Math.pow(y1-y2, 2)));
    }
}
