/*****************************************************************************
 * Copyright 2007-2015 DCA-FEEC-UNICAMP
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * Contributors:
 *    Klaus Raizer, Andre Paraense, Ricardo Ribeiro Gudwin
 *****************************************************************************/

package codelets.behaviors;

import br.unicamp.cst.core.entities.Codelet;
import br.unicamp.cst.core.entities.MemoryContainer;
import br.unicamp.cst.core.entities.MemoryObject;
import java.util.List;
import memory.CreatureInnerSense;
import org.json.JSONException;
import org.json.JSONObject;
import ws3dproxy.model.Leaflet;
import ws3dproxy.model.Thing;

/** 
 * 
 * @author klaus
 * 
 * 
 */

public class Forage extends Codelet {
    
        private MemoryObject knownMO;
        private List<Thing> known;
        private MemoryContainer legsMO;
        private MemoryObject knownJewelsMO;
        private MemoryObject innerSenseMO;
        private List<Thing> knownJ;
        private int indice = -1;
        private double eval = 0.5;
        public List<Leaflet> leaflets;
	/**
	 * Default constructor
	 */
	public Forage(){       
	}

	@Override
	public void proc() {
            CreatureInnerSense cis = (CreatureInnerSense) innerSenseMO.getI();
            leaflets = cis.leaflet; 
        
            known = (List<Thing>) knownMO.getI();
            knownJ = (List<Thing>) knownJewelsMO.getI();
            
            int qntFalta = -1;
            if (leaflets != null){
                qntFalta = IsLeafletMissingJewel("Red") + IsLeafletMissingJewel("Green") +  IsLeafletMissingJewel("Blue") + IsLeafletMissingJewel("Yellow") + IsLeafletMissingJewel("Magenta") +  IsLeafletMissingJewel("White");
                if (qntFalta == 0){//leaflet Completo
                    eval = 1.5;
                    JSONObject message=new JSONObject();
			try {
                            message.put("ACTION", "FORAGE");
                            if(indice == -1)
                                indice = legsMO.setI(message.toString());
                            else
                                legsMO.setI(message.toString(), eval, indice);
                            //legsMO.setI(message.toString());
			
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
                }
            }
            //System.out.println("faltam: " + qntFalta);
            
            if (qntFalta != 0){
                if (known.size() == 0 || knownJ.size() == 0) {
                    JSONObject message=new JSONObject();
                            try {
                                    message.put("ACTION", "FORAGE");
                                    if(indice == -1)
                                        indice = legsMO.setI(message.toString());
                                    else
                                        legsMO.setI(message.toString(), eval, indice);
                                    //legsMO.setI(message.toString());

                            } catch (JSONException e) {
                                    // TODO Auto-generated catch block
                                    e.printStackTrace();
                            }
                }   
            }         
		
	}

	@Override
	public void accessMemoryObjects() {
            knownMO = (MemoryObject)this.getInput("KNOWN_APPLES");
            legsMO=(MemoryContainer)this.getOutput("LEGS");
            knownJewelsMO = (MemoryObject)this.getInput("KNOWN_JEWELS");
            innerSenseMO = (MemoryObject)this.getInput("INNER");

            //if (innerSenseMO == null) System.out.println("knownJewelsMO is null");
		// TODO Auto-generated method stub
		
	}
        
        @Override
        public void calculateActivation() {
            
        }

        public int IsLeafletMissingJewel(String color) {
            int missingofthiscolor = 0;
            //System.out.println("cor " + color);
            for (Leaflet l : leaflets) {
                    int i = l.getMissingNumberOfType(color);
                    if (i >= 0) {
                            missingofthiscolor += i;
                    }
            }
            //System.out.println("falta " + missingofthiscolor);
            return (missingofthiscolor);
        }   
}
