/*
 * Copyright (C) 2018 leticia.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301  USA
 */
package codelets.behaviors;

import br.unicamp.cst.core.entities.Codelet;
import br.unicamp.cst.core.entities.MemoryContainer;
import br.unicamp.cst.core.entities.MemoryObject;
import java.awt.Point;
import java.awt.geom.Point2D;
import memory.CreatureInnerSense;
import org.json.JSONException;
import org.json.JSONObject;
import ws3dproxy.model.Thing;

/**
 *
 * @author leticia
 */
public class GoToJewel extends Codelet{

    private MemoryObject closestJewelMO;
	private MemoryObject selfInfoMO;
	private MemoryContainer legsMO;
	private int creatureBasicSpeed;
	private double reachDistance;
        private int indice = -1;
        private double eval;
        
    public GoToJewel(int creatureBasicSpeed, int reachDistance){
        this.creatureBasicSpeed=creatureBasicSpeed;
	this.reachDistance=reachDistance;
    }
    
    @Override
    public void accessMemoryObjects() {
        closestJewelMO = (MemoryObject)this.getInput("CLOSEST_LEAFLETJEWEL");
        selfInfoMO=(MemoryObject)this.getInput("INNER");
        legsMO=(MemoryContainer)this.getOutput("LEGS");
    }

    @Override
    public void calculateActivation() {

    }

    @Override
    public void proc() {
        // Find distance between creature and closest apple
        //If far, go towards it
        //If close, stops
        eval = 0.0;
        Thing closestJewel = (Thing) closestJewelMO.getI();
        CreatureInnerSense cis = (CreatureInnerSense) selfInfoMO.getI();

        if(closestJewel != null)
        {
                double jewelX=0;
                double jewelY=0;
                try {
                        jewelX = closestJewel.getX1();
                        jewelY = closestJewel.getY1();

                } catch (Exception e) {
                        e.printStackTrace();
                }

                double selfX=cis.position.getX();
                double selfY=cis.position.getY();

                Point2D pApple = new Point();
                pApple.setLocation(jewelX, jewelY);

                Point2D pSelf = new Point();
                pSelf.setLocation(selfX, selfY);

                double distance = pSelf.distance(pApple);
                JSONObject message=new JSONObject();
                try {
                        if(distance>reachDistance){ //Go to it
                            message.put("ACTION", "GOTO");
                            message.put("X", (int)jewelX);
                            message.put("Y", (int)jewelY);
                            message.put("SPEED", creatureBasicSpeed);	

                        }else{//Stop
                            message.put("ACTION", "GOTO");
                            message.put("X", (int)jewelX);
                            message.put("Y", (int)jewelY);
                            message.put("SPEED", 0.0);	
                        }
                        if(cis.fuel >= 400)
                            eval = 1.0;
                        else
                            eval = 0.0;
                        
                        if(indice == -1)
                            indice = legsMO.setI(message.toString());
                        else
                            legsMO.setI(message.toString(), eval, indice);
                } catch (JSONException e) {
                        e.printStackTrace();
                }	
        }
    }
    
}
