/*****************************************************************************
 * Copyright 2007-2015 DCA-FEEC-UNICAMP
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * Contributors:
 *    Klaus Raizer, Andre Paraense, Ricardo Ribeiro Gudwin
 *****************************************************************************/

package codelets.behaviors;

import java.awt.Point;
import java.awt.geom.Point2D;

import org.json.JSONException;
import org.json.JSONObject;

import br.unicamp.cst.core.entities.Codelet;
import br.unicamp.cst.core.entities.MemoryContainer;
import br.unicamp.cst.core.entities.MemoryObject;
import java.util.Collections;
import memory.CreatureInnerSense;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;
import ws3dproxy.model.Thing;

public class EatClosestApple extends Codelet {

	private MemoryObject closestAppleMO;
	private MemoryObject innerSenseMO;
        private MemoryObject knownMO;
        private MemoryObject knownAllApplesMO;
	private int reachDistance;
	private MemoryContainer handsMO;
        Thing closestApple;
        CreatureInnerSense cis;
        List<Thing> known;
        List<Thing> knownApples;
        private int indice = -1;
        private double eval;

	public EatClosestApple(int reachDistance) {
                setTimeStep(50);
		this.reachDistance=reachDistance;
	}

	@Override
	public void accessMemoryObjects() {
            closestAppleMO=(MemoryObject)this.getInput("CLOSEST_APPLE");
            innerSenseMO=(MemoryObject)this.getInput("INNER");
            handsMO=(MemoryContainer)this.getOutput("HANDS");
            knownMO = (MemoryObject)this.getOutput("KNOWN_APPLES");
            knownAllApplesMO = (MemoryObject)this.getInput("KNOWN_APPLES");
	}

	@Override
	public void proc() {
                String appleName="";
                closestApple = (Thing) closestAppleMO.getI();
                cis = (CreatureInnerSense) innerSenseMO.getI();
                known = (List<Thing>) knownMO.getI();
		//Find distance between closest apple and self
		//If closer than reachDistance, eat the apple
		//eval = 0.0;
		if(closestApple != null)
		{
			double appleX=0;
			double appleY=0;
			try {
				appleX=closestApple.getX1();
				appleY=closestApple.getY1();
                                appleName = closestApple.getName();
                                

			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			double selfX=cis.position.getX();
			double selfY=cis.position.getY();

			Point2D pApple = new Point();
			pApple.setLocation(appleX, appleY);

			Point2D pSelf = new Point();
			pSelf.setLocation(selfX, selfY);

			double distance = pSelf.distance(pApple);
			JSONObject message=new JSONObject();
                        
                        if(cis.fuel < 400)
                            eval = 1.0;
                        else
                            eval = 0.0;
			try {
                               if(distance<reachDistance){ //eat it						
					message.put("OBJECT", appleName);
					message.put("ACTION", "EATIT");
					if(indice == -1)
                                            indice = handsMO.setI(message.toString());
                                        else
                                            handsMO.setI(message.toString(), eval, indice);
                                        DestroyClosestApple();
				}else{
                                   boolean flag = true;
                                   knownApples = Collections.synchronizedList((List<Thing>) knownAllApplesMO.getI());
                                    synchronized(knownApples) {
                                        if(knownApples.size() != 0){
                                             //Iterate over objects in vision, looking for the closest apple
                                             CopyOnWriteArrayList<Thing> myknown = new CopyOnWriteArrayList<>(knownApples);
                                             for (Thing t : myknown) {
                                                String objectName=t.getName();
                                                //if(objectName.contains("PFood") && !objectName.contains("NPFood")){ //Then, it is an apple     
                                                    double Dnew = calculateDistance(t.getX1(), t.getY1(), cis.position.getX(), cis.position.getY());
                                                    if(Dnew < reachDistance){
                                                        flag = false;
                                                        eval = 1.2;
                                                        message.put("OBJECT", t.getName());
                                                        message.put("ACTION", "EATIT");
                                                        if(indice == -1)
                                                            indice = handsMO.setI(message.toString());
                                                        else
                                                            handsMO.setI(message.toString(), eval, indice);
                                                        DestroyClosestApple(t);
                                                    }   
                                                //}
                                             }
                                    }//fim syncronized
                                    if (flag){
                                        if(indice == -1)
                                            indice = handsMO.setI("");
                                        else
                                            handsMO.setI("", eval, indice);//nothing
                                    }
				}
                               }
//				System.out.println(message);
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}else{
			if(indice == -1)
                            indice = handsMO.setI("");
                        else
                            handsMO.setI("", eval, indice);//nothing
		}
        //System.out.println("Before: "+known.size()+ " "+known);
        
        //System.out.println("After: "+known.size()+ " "+known);
	//System.out.println("EatClosestApple: "+ handsMO.getInfo());	

	}
        
        @Override
        public void calculateActivation() {
        
        }
        
        public void DestroyClosestApple() {
           int r = -1;
           int i = 0;
           synchronized(known) {
             CopyOnWriteArrayList<Thing> myknown = new CopyOnWriteArrayList<>(known);  
             for (Thing t : known) {
              if (closestApple != null) 
                 if (t.getName().equals(closestApple.getName())) r = i;
              i++;
             }   
             if (r != -1) known.remove(r);
             closestApple = null;
           }
        }
        
        public void DestroyClosestApple(Thing apple) {
           int r = -1;
           int i = 0;
           synchronized(knownApples) {
             CopyOnWriteArrayList<Thing> myknown = new CopyOnWriteArrayList<>(knownApples);  
             for (Thing t : knownApples) {
              //if (closestApple != null) 
                 if (t.getName().equals(apple.getName())) r = i;
              i++;
             }   
             if (r != -1) knownApples.remove(r);
             //closestApple = null;
           }
        }
        
        private double calculateDistance(double x1, double y1, double x2, double y2) {
            return(Math.sqrt(Math.pow(x1-x2, 2)+Math.pow(y1-y2, 2)));
        }

}
