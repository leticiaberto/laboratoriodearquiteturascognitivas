/*
 * Copyright (C) 2018 leticia.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301  USA
 */
package codelets.perception;

import br.unicamp.cst.core.entities.Codelet;
import br.unicamp.cst.core.entities.MemoryObject;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;
import memory.CreatureInnerSense;
import ws3dproxy.model.Creature;
import ws3dproxy.model.Leaflet;
import ws3dproxy.model.Thing;
/**
 *
 * @author leticia
 */
public class ClosestLeafletJewelDetector extends Codelet {
    private MemoryObject knownMO;
    private MemoryObject closestLeafletJewelMO;
    private MemoryObject innerSenseMO;
    
    private List<Thing> known;
    public List<Leaflet> leaflets;
    
    public ClosestLeafletJewelDetector(){
        
    }
    
    @Override
    public void accessMemoryObjects() {
        this.knownMO = (MemoryObject)this.getInput("KNOWN_JEWELS");
        this.innerSenseMO = (MemoryObject)this.getInput("INNER");
        this.closestLeafletJewelMO = (MemoryObject)this.getOutput("CLOSEST_LEAFLETJEWEL");
    }

    @Override
    public void calculateActivation() {
        
    }

    @Override
    public void proc() {
        //Thing closest_leafletJewel = null;
        known = Collections.synchronizedList((List<Thing>) knownMO.getI());
        CreatureInnerSense cis = (CreatureInnerSense) innerSenseMO.getI();
        
        //pegar leaflets
        Thing closestJewel = null;
        Thing closestLeafletJewel = null;
        leaflets = cis.leaflet;      
 
        synchronized(known) {
           if(known.size() != 0){
                //Iterate over objects in vision, looking for the closest apple
                CopyOnWriteArrayList<Thing> myknown = new CopyOnWriteArrayList<>(known);
                for (Thing t : myknown) {
                    String objectName=t.getName();
                    if(objectName.contains("Jewel")){ //Then, it is an jewel
                        double Dnew = calculateDistance(t.getX1(), t.getY1(), cis.position.getX(), cis.position.getY());//distancia da joia sendo vista

                        if(closestJewel == null){
                            closestJewel = t;
                        }
                        else {
                            double Dclosest= calculateDistance(closestJewel.getX1(), closestJewel.getY1(), cis.position.getX(), cis.position.getY());    
                            if( Dnew < Dclosest) {
                                closestJewel = t;
                            }
                        }
                        
                         if(closestLeafletJewel == null && IsLeafletMissingJewel(t.getMaterial().getColorName())){
                            closestLeafletJewel = t;
                         } 
                         else if(closestLeafletJewel != null && IsLeafletMissingJewel(t.getMaterial().getColorName()))
                         {
                            double DclosestLeaflet= calculateDistance(closestLeafletJewel.getX1(), closestLeafletJewel.getY1(), cis.position.getX(), cis.position.getY());
                                if(Dnew < DclosestLeaflet){
                                    closestLeafletJewel = t;
                                }
                         }
                    }
                }

                if (closestLeafletJewel != null) {
                    closestJewel = closestLeafletJewel;
                }
                
                if(closestJewel!= null){    
                    if(closestLeafletJewelMO.getI() == null || !closestLeafletJewelMO.getI().equals(closestJewel)){
                       closestLeafletJewelMO.setI(closestJewel);
                    }

                }else{
                    //couldn't find any nearby apples
                    closestJewel = null;
                    closestLeafletJewelMO.setI(closestJewel);
                }
           }
           else  { // if there are no known apples closest_apple must be null
                closestJewel = null;
                closestLeafletJewelMO.setI(closestJewel);
           }
        }
    }
    
    private double calculateDistance(double x1, double y1, double x2, double y2) {
            return(Math.sqrt(Math.pow(x1-x2, 2)+Math.pow(y1-y2, 2)));
        }
    
    public Boolean IsLeafletMissingJewel(String color) {
        int missingofthiscolor = 0;
        for (Leaflet l : leaflets) {
                int i = l.getMissingNumberOfType (color);
                if (i >= 0) {
                        missingofthiscolor += i;
                }
        }
        return (missingofthiscolor > 0);
    }
    
}
