/*
 * Copyright (C) 2018 leticia.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301  USA
 */
package codelets.perception;

import br.unicamp.cst.core.entities.Codelet;
import br.unicamp.cst.core.entities.MemoryObject;
import java.util.ArrayList;
import java.util.List;
import memory.CreatureInnerSense;
import ws3dproxy.model.Creature;
import ws3dproxy.model.Thing;
import ws3dproxy.model.World;
import ws3dproxy.model.WorldPoint;
import ws3dproxy.util.Constants;

/**
 *
 * @author leticia
 */
public class PlannerDetector extends Codelet{

    private MemoryObject gridMO;
    private MemoryObject innerMO;
    private MemoryObject allPlanMO;
    private MemoryObject gridAtualizadoMO;
    
    private Creature c;
    
    private int [][] matrizCenario = new int[12][16];
    double x = 0, y = 0;
    
    List<WorldPoint> allPlan;
    
    private double xDest = 800;
    private double yDest = 600;
    boolean flag = true;
    
    public PlannerDetector(Creature nc){
        allPlan = new ArrayList<WorldPoint>();
        c = nc;
        x = nc.getPosition().getX();
        y = nc.getPosition().getY();
        
          /*TEEEEEEEEEEEEEEEEEESTE*/
       

        double x1, x2, y1, y2, posX, posY;
        try{
            List<Thing> worldEntities = World.getWorldEntities();
            for(Thing t: worldEntities)
                if (t.getCategory() == Constants.categoryBRICK){
                    x1 = t.getX1();
                    x2 = t.getX2();
                    y1 = t.getY1();
                    y2 = t.getY2();
                    posX = (x2 - x1) / 50;
                    posY = (y2 - y1) / 50;
                    //System.out.println("X = " + x1 + " Y = "+ y1);
                   // System.out.println("PosX = " + posX + " PosY = "+ posY);
                    int i = (int) y1 / 50;//posição inicial linha 
                    int j = (int) x1 / 50;//posição inicial coluna
                                
                    if(posY > 1.00)//coluna fixa (parede na vertical)
                       for(double k = y1; k < y2; k = k + 50){
                           matrizCenario[i][j] = 1;
                           i++;
                       }
                    else//linha fica (parede na horizontal)
                        for(double k = x1; k < x2; k = k + 50){
                            matrizCenario[i][j] = 1; 
                            j++;
                        }    
                }
            //posição da criatura na matriz
            double xC, yC;
            xC = x;
            yC = y;
            matrizCenario[(int) xC/50][(int) yC/50] = 2;
            int iniX = (int) xC/50;
            int iniY = (int) yC/50;
            
         
            //gridMO.setI(matrizCenario);
        }catch (Exception e) {
            e.printStackTrace();
        }
        
        /*FIM TEEEEEEEEEEESTE*/
    }
    @Override
    public void accessMemoryObjects() {
        gridMO = (MemoryObject)this.getInput("GRID");
        innerMO = (MemoryObject)this.getInput("INNER");
        allPlanMO = (MemoryObject)this.getOutput("ALL_PLAN");
        gridAtualizadoMO = (MemoryObject)this.getOutput("GRID");
    }

    @Override
    public void calculateActivation() {
    }

    @Override
    public void proc() {
        
        //matrizCenario = (int [][]) gridMO.getI();
        CreatureInnerSense cis = (CreatureInnerSense) innerMO.getI();
             

        /*System.out.println("pLANNER"); 
        for(int i = 0; i < 12; i++){
                for(int j = 0; j < 16; j++){
                    System.out.print(matrizCenario[i][j] + " "); 
                }
                System.out.println(); 
        }*/
        
        double xC, yC;
        //x = c.getPosition().getX();
        //y = c.getPosition().getY();
        
        xC = c.getPosition().getX();
        yC = c.getPosition().getY();
       
        //System.out.println("XC: " + xC + " YC: " + yC);
        int i = 0;
        int j = 0;
        double min = 0;
        double minCal = 0;
        int minI = 0, minJ = 0;
        
        for(i = 0; i < 12; i++){
            for(j = 0; j < 16; j++){
                if(matrizCenario[i][j] == 2){
                    minI = i;
                    minJ = j;
                    break;
                }
            }
        }

        i = minI;
        j = minJ;

        if(i == 11 && j == 15 && flag ){
            double mapX, mapY;
            for(i = 0; i < 12; i++){
                for(j = 0; j < 16; j++){
                    if(matrizCenario[i][j] == 3 || matrizCenario[i][j] == 2){
                        mapX = j * 50;
                        mapY = i * 50;
                        allPlan.add(new WorldPoint(mapX, mapY));
                    }
                }
            }
            flag = false;
            allPlanMO.setI(allPlan);
        }
        else{
            minI = i;
            minJ = j+1;

           if(j+1 < 16){
                if(matrizCenario[i][j+1] == 0)
                    min = Math.sqrt(Math.pow((xDest - xC),2) + Math.pow((yDest - yC + 50),2));
           }

           if(j-1 >= 0){
                if(matrizCenario[i][j-1] == 0){
                    minCal = Math.sqrt(Math.pow((xDest - xC),2) + Math.pow((yDest - yC - 50),2));
                     if (minCal > min){
                         min = minCal;
                         minI = i;
                         minJ = j-1;
                     }
                }
           }
           
           if(i+1 < 12){
                if(matrizCenario[i+1][j] == 0){
                    minCal = Math.sqrt(Math.pow((xDest - xC + 50),2) + Math.pow((yDest - yC),2));
                    if (minCal > min){
                        min = minCal;
                        minI = i + 1;
                        minJ = j;
                    }
                }
           }
           
           if (i-1 >= 0){
                if(matrizCenario[i-1][j] == 0){
                   minCal = Math.sqrt(Math.pow((xDest - xC - 50),2) + Math.pow((yDest - yC),2));
                    if (minCal > min){
                        min = minCal;
                        minI = i - 1;
                        minJ = j;
                    }
                }
           }

            matrizCenario[i][j] = 3;
            matrizCenario[minI][minJ] = 2;
            
           /* System.out.println("pLANNER"); 
        for(i = 0; i < 12; i++){
                for(j = 0; j < 16; j++){
                    System.out.print(matrizCenario[i][j] + " "); 
                }
                System.out.println(); 
        }*/
            
            if(minI == i)
                y = yC;
            else if(minI == i + 1){
                y = yC + 50.00;
                if(y % 50.00 != 0)
                    y = y + (50.00 - y % 50.00);
            }
            else if(minI == i - 1){
                    y = yC - 50.00;
                    if(y % 50.00 != 0)
                        y = y - (50.00 + y % 50.00);
            }

            if(minJ == j)
                x = xC;
            else if(minJ == j + 1){
                x = xC + 50.00;
                if(x % 50.00 != 0)
                    x = x + (50.00 - x % 50.00);
            }
            else if(minJ == j - 1){
                x = xC - 50.00;
                 if(x % 50.00 != 0)
                    x = x - (50.00 - x % 50.00);
            }


            /*int delta = 1;
            while ((x - xC) > delta || (y - yC) > delta){
               creature.moveto(1.0, x, y);
               creature.updateState();
               xC = creature.getPosition().getX();
               yC = creature.getPosition().getY();
               creature.move(0.0, 0.0, 0.0);
            }*/
            //System.out.println("X: " + x + " Y: " + y);
            
            //c.getPosition().setX(x);
            //c.getPosition().setY(y);
            //c.updateState();
            //System.out.println("PosX: " + c.getPosition().getX() + " PosY: " + c.getPosition().getY());
            gridAtualizadoMO.setI(matrizCenario);

        }//fim else
        
    }
    
}
