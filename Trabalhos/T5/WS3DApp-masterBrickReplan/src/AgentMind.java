/*****************************************************************************
 * Copyright 2007-2015 DCA-FEEC-UNICAMP
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * Contributors:
 *    Klaus Raizer, Andre Paraense, Ricardo Ribeiro Gudwin
 *****************************************************************************/

import br.unicamp.cst.core.entities.Codelet;
import br.unicamp.cst.core.entities.MemoryObject;
import br.unicamp.cst.core.entities.Mind;
import codelets.behaviors.GoToNextStep;
import codelets.behaviors.TargetCompleted;
import codelets.motor.LegsActionCodelet;
import codelets.perception.ExecuteDetector;
import codelets.perception.PlannerDetectorMelhor;
import codelets.sensors.GameMap;
import codelets.sensors.InnerSense;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import memory.CreatureInnerSense;
import support.MindView;
import ws3dproxy.model.WorldPoint;

/**
 *
 * @author rgudwin
 */
public class AgentMind extends Mind {
    
    private static int creatureBasicSpeed=1;
    private static int reachDistance = 8;
    
    public AgentMind(Environment env) {
                super();
                
                // Declare Memory Objects
	        MemoryObject legsMO;
                MemoryObject innerSenseMO;
                MemoryObject gridMO;
                MemoryObject allPlanMO;
                MemoryObject nextStepPlanMO;
                
                //Initialize Memory Objects
                legsMO=createMemoryObject("LEGS", "");
                
                CreatureInnerSense cis = new CreatureInnerSense();
		innerSenseMO=createMemoryObject("INNER", cis);
                
                int [][] grid = new int[12][16]; 
                gridMO = createMemoryObject("GRID", grid);
                
                List<WorldPoint> allPlan = Collections.synchronizedList(new ArrayList<WorldPoint>());
                allPlanMO = createMemoryObject("ALL_PLAN", allPlan);
                
                WorldPoint nextStep = null;
                nextStepPlanMO = createMemoryObject("NEXT_STEP", nextStep);
                
                // Create and Populate MindViewer
                MindView mv = new MindView("MindView");
                mv.addMO(allPlanMO);
                mv.addMO(nextStepPlanMO);
                mv.addMO(innerSenseMO);
                mv.addMO(legsMO);
                mv.StartTimer();
                mv.setVisible(true);
				
		Codelet innerSense=new InnerSense(env.c);
		innerSense.addOutput(innerSenseMO);
                insertCodelet(innerSense); //A sensor for the inner state of the creature
		
                Codelet gameMap = new GameMap(env.c);
                gameMap.addOutput(gridMO);
                gameMap.addInput(innerSenseMO);
                insertCodelet(gameMap);
                
                                
                Codelet planM= new PlannerDetectorMelhor(env.c);
                planM.addInput(gridMO);
                planM.addInput(innerSenseMO);
                planM.addOutput(allPlanMO);
                planM.addOutput(gridMO);
                insertCodelet(planM);
                
                Codelet execute = new ExecuteDetector();
                execute.addInput(allPlanMO);
                execute.addOutput(nextStepPlanMO);
                insertCodelet(execute);
                
                Codelet goToStep = new GoToNextStep(creatureBasicSpeed,reachDistance);
                goToStep.addInput(nextStepPlanMO);
                goToStep.addInput(innerSenseMO);
                goToStep.addOutput(allPlanMO);
                goToStep.addOutput(legsMO);
                insertCodelet(goToStep);
                
                              
                Codelet target = new TargetCompleted(reachDistance);
                target.addInput(nextStepPlanMO);
                target.addInput(innerSenseMO);
                target.addOutput(allPlanMO);
                insertCodelet(target);
                
		// Create Actuator Codelets
		Codelet legs=new LegsActionCodelet(env.c);
		legs.addInput(legsMO);
                insertCodelet(legs);

		
                // sets a time step for running the codelets to avoid heating too much your machine
                for (Codelet c : this.getCodeRack().getAllCodelets())
                    c.setTimeStep(200);
		
		// Start Cognitive Cycle
		start(); 
    }             
    
}
