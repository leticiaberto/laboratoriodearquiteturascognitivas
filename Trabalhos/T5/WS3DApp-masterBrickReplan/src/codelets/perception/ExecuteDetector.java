/*
 * Copyright (C) 2018 leticia.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301  USA
 */
package codelets.perception;

import br.unicamp.cst.core.entities.Codelet;
import br.unicamp.cst.core.entities.MemoryObject;
import java.util.Collections;
import java.util.List;
import ws3dproxy.model.WorldPoint;

/**
 *
 * @author leticia
 */
public class ExecuteDetector extends Codelet{

    private MemoryObject allPlanMO;
    private MemoryObject nextStepPlanMO;
    
    List<WorldPoint> plan;
    
    public ExecuteDetector(){
        
    }
    @Override
    public void accessMemoryObjects() {
        allPlanMO = (MemoryObject)this.getInput("ALL_PLAN");
        nextStepPlanMO = (MemoryObject)this.getOutput("NEXT_STEP");
    }

    @Override
    public void calculateActivation() {
    }

    @Override
    public void proc() {
        plan = Collections.synchronizedList((List<WorldPoint>) allPlanMO.getI());
        
        synchronized(plan) {
		   if(plan.size() != 0){
                       //System.out.println("executor");
                       nextStepPlanMO.setI(plan.get(0));
                   }
                   else
                       nextStepPlanMO.setI(null);
        }
    }
    
}
