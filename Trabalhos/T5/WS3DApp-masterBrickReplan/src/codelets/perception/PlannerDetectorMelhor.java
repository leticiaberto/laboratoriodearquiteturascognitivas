/*
 * Copyright (C) 2018 leticia.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301  USA
 */
package codelets.perception;

import br.unicamp.cst.core.entities.Codelet;
import br.unicamp.cst.core.entities.MemoryObject;
import static com.sun.org.apache.xalan.internal.xsltc.compiler.util.Type.Int;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import memory.CreatureInnerSense;
import ws3dproxy.model.Creature;
import ws3dproxy.model.Thing;
import ws3dproxy.model.World;
import ws3dproxy.model.WorldPoint;
import ws3dproxy.util.Constants;

/**
 *
 * @author leticia
 */
public class PlannerDetectorMelhor extends Codelet{

    private MemoryObject gridMO;
    private MemoryObject innerMO;
    private MemoryObject allPlanMO;
    private MemoryObject gridAtualizadoMO;
    
    private Creature c;
    
    private int [][] matrizCenario = new int[12][16];
    double x = 0, y = 0;
    
    List<WorldPoint> allPlan;
    
    private double xDest = 800;
    private double yDest = 600;
    boolean flag = true;
    
    List<Integer> previousI;
    List<Integer> previousJ;
    
    int tam = 1;
    
    public PlannerDetectorMelhor(Creature nc){
        allPlan = new ArrayList<WorldPoint>();
        previousI = new ArrayList<>();
        previousJ = new ArrayList<>();
        c = nc;
        x = nc.getPosition().getX();
        y = nc.getPosition().getY();
        
          /*TEEEEEEEEEEEEEEEEEESTE*/
       for(int i = 0; i < 12; i++)
           for (int j = 0; j < 16; j++)
               matrizCenario[i][j] = 0;

        for(int j = 0; j < 16; j++){
            matrizCenario[1][j] = 1;
            matrizCenario[0][j] = 1;
        }
        matrizCenario[2][2] = 2;
        
        for(int j = 1; j < 16; j++){
            matrizCenario[3][j] = 1;
        }
        
        for(int i = 3; i < 10; i++)
            matrizCenario[i][1] = 1;
        
        matrizCenario[11][2] = 1;
        /*for(int i = 0; i < 12; i++){
                for(int j = 0; j < 16; j++){
                    System.out.print(matrizCenario[i][j] + " "); 
                }
                System.out.println(); 
        }*/
        
        /*FIM TEEEEEEEEEEESTE*/
    }
    @Override
    public void accessMemoryObjects() {
        gridMO = (MemoryObject)this.getInput("GRID");
        innerMO = (MemoryObject)this.getInput("INNER");
        allPlanMO = (MemoryObject)this.getOutput("ALL_PLAN");
        gridAtualizadoMO = (MemoryObject)this.getOutput("GRID");
    }

    @Override
    public void calculateActivation() {
    }

    @Override
    public void proc() {
        
        //matrizCenario = (int [][]) gridMO.getI();
        CreatureInnerSense cis = (CreatureInnerSense) innerMO.getI();
             

        System.out.println("Planner"); 
        /*for(int i = 0; i < 12; i++){
                for(int j = 0; j < 16; j++){
                    System.out.print(matrizCenario[i][j] + " "); 
                }
                System.out.println(); 
        }*/
        
        double xC, yC;
        //x = c.getPosition().getX();
        //y = c.getPosition().getY();
        
        xC = c.getPosition().getX();
        yC = c.getPosition().getY();
       
        //System.out.println("XC: " + xC + " YC: " + yC);
        int i = 0;
        int j = 0;
        double min = 0;
        double minCal = 0;
        int minI = 0, minJ = 0;
        
        for(i = 0; i < 12; i++){
            for(j = 0; j < 16; j++){
                if(matrizCenario[i][j] == 2){
                    minI = i;
                    minJ = j;
                    break;
                }
            }
        }

        i = minI;
        j = minJ;

        if(i == 11 && j == 15 && flag){
            double mapX, mapY;
            
            for(j = 15; j >= 0; j--){
                if(matrizCenario[2][j] == 3 || matrizCenario[2][j] == 2){
                    mapX = j * 50;
                    mapY = 2 * 50;
                    //System.out.println("mapX: " + mapX + " MapY: " + mapY);
                    allPlan.add(new WorldPoint(mapX, mapY));
                }
            }
            
            
            for(i = 3; i < 10; i++){
                for(j = 0; j < 16; j++){
                    if(matrizCenario[i][j] == 3 || matrizCenario[i][j] == 2){
                        mapX = j * 50;
                        mapY = i * 50;
                        allPlan.add(new WorldPoint(mapX, mapY));
                    }
                }
            }
            if(matrizCenario[10][0] == 3 || matrizCenario[10][0] == 2){
                mapX = 0 * 50;
                mapY = 10 * 50;
                allPlan.add(new WorldPoint(mapX, mapY));
            }
            if(matrizCenario[11][0] == 3 || matrizCenario[11][0] == 2){
                mapX = 0 * 50;
                mapY = 11 * 50;
                allPlan.add(new WorldPoint(mapX, mapY));
            }
            if(matrizCenario[11][1] == 3 || matrizCenario[11][1] == 2){
                mapX = 1 * 50;
                mapY = 11 * 50;
                allPlan.add(new WorldPoint(mapX, mapY));
            }
            
                for(j = 1; j < 16; j++){
                    if(matrizCenario[10][j] == 3 || matrizCenario[10][j] == 2){
                        mapX = j * 50;
                        mapY = 10 * 50;
                        allPlan.add(new WorldPoint(mapX, mapY));
                    }
                }
                for(j = 2; j < 16; j++){
                    if(matrizCenario[11][j] == 3 || matrizCenario[11][j] == 2){
                        mapX = j * 50;
                        mapY = 11 * 50;
                        allPlan.add(new WorldPoint(mapX, mapY));
                    }
                }
            
            flag = false;
            allPlanMO.setI(allPlan);
        }
        else if(flag){
            minI = i;
            minJ = j+1;
            
            if(j+1 < 16){
                if(matrizCenario[i][j+1] == 0)
                    min = Math.sqrt(Math.pow((xDest - xC),2) + Math.pow((yDest - yC + 50),2));
           }

           if(j-1 >= 0){
                if(matrizCenario[i][j-1] == 0){
                    minCal = Math.sqrt(Math.pow((xDest - xC),2) + Math.pow((yDest - yC - 50),2));
                     if (minCal > min){
                         min = minCal;
                         minI = i;
                         minJ = j-1;
                     }
                }
           }
           
           if(i+1 < 12){
                if(matrizCenario[i+1][j] == 0){
                    minCal = Math.sqrt(Math.pow((xDest - xC + 50),2) + Math.pow((yDest - yC),2));
                    if (minCal > min){
                        min = minCal;
                        minI = i + 1;
                        minJ = j;
                    }
                }
           }
           
           if (i-1 >= 0){
                if(matrizCenario[i-1][j] == 0){
                   minCal = Math.sqrt(Math.pow((xDest - xC - 50),2) + Math.pow((yDest - yC),2));
                    if (minCal > min){
                        min = minCal;
                        minI = i - 1;
                        minJ = j;
                    }
                }
           }
           boolean repI = true;
           boolean repJ = true;
           
           /*for (Integer indI: previousI){
               if(indI.equals(minI)){
                   repI = false;
                   System.out.println("minI" + minI);
               }
           }
           for (Integer indJ: previousI){
               if(indJ.equals(minJ))
                   repJ = false;
           }*/
           if(minJ == 16)
               repJ = false;
           else if(minJ == j+ 1 && matrizCenario[i][j+1] != 0)
               repJ = false;
           //esta tentando voltar para uma posição que ja passou - logo nao tem caminho por essa rota
           /*if (repI == false && repJ == false){
               System.out.println("I: " + previousI.get(previousI.size()-1) + " J " + previousJ.get(previousJ.size()-1));
               matrizCenario[i][j] = 5;
               matrizCenario[previousI.get(previousI.size()-tam)][previousJ.get(previousJ.size()-tam)] = 2;
               //tam = tam + 1;
               //previousI.remove(previousI.size());
               //previousJ.remove(previousJ.size());
           }*/
           if(repJ == false){
               matrizCenario[i][j] = 5;
               matrizCenario[previousI.get(previousI.size()-1)][previousJ.get(previousJ.size()-1)] = 2;
               previousI.remove(previousI.size()-1);
               previousJ.remove(previousJ.size()-1);
                for(i = 0; i < 12; i++){
                    for(j = 0; j < 16; j++){
                        System.out.print(matrizCenario[i][j] + " "); 
                    }
                    System.out.println(); 
                }
           }
           else{
                matrizCenario[i][j] = 3;
                matrizCenario[minI][minJ] = 2;

                //armazena o i e j da iteração que levou a próxima posição
                previousI.add(i);
                previousJ.add(j);

                
                for(i = 0; i < 12; i++){
                    for(j = 0; j < 16; j++){
                        System.out.print(matrizCenario[i][j] + " "); 
                    }
                    System.out.println(); 
                }

                if(minI == i)
                    y = yC;
                else if(minI == i + 1){
                    y = yC + 50.00;
                    if(y % 50.00 != 0)
                        y = y + (50.00 - y % 50.00);
                }
                else if(minI == i - 1){
                        y = yC - 50.00;
                        if(y % 50.00 != 0)
                            y = y - (50.00 + y % 50.00);
                }

                if(minJ == j)
                    x = xC;
                else if(minJ == j + 1){
                    x = xC + 50.00;
                    if(x % 50.00 != 0)
                        x = x + (50.00 - x % 50.00);
                }
                else if(minJ == j - 1){
                    x = xC - 50.00;
                     if(x % 50.00 != 0)
                        x = x - (50.00 - x % 50.00);
                } 
            }  
            gridAtualizadoMO.setI(matrizCenario);

        }//fim else
        
    }
    
}
