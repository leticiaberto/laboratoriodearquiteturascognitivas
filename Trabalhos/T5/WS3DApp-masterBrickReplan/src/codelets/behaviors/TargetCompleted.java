/*
 * Copyright (C) 2018 leticia.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301  USA
 */
package codelets.behaviors;

import br.unicamp.cst.core.entities.Codelet;
import br.unicamp.cst.core.entities.MemoryObject;
import java.awt.Point;
import java.awt.geom.Point2D;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;
import memory.CreatureInnerSense;
import org.json.JSONException;
import org.json.JSONObject;
import ws3dproxy.model.WorldPoint;

/**
 *
 * @author leticia
 */
public class TargetCompleted extends Codelet {
    
    private int reachDistance;
    private MemoryObject nextStepMO;
    private MemoryObject allPlanMO;
    private MemoryObject selfInfoMO;
    
    CreatureInnerSense cis;
    WorldPoint step;
    List<WorldPoint> plan;
    
    public TargetCompleted(int reachDistance){
        this.reachDistance = reachDistance;
    }
    @Override
    public void accessMemoryObjects() {
        nextStepMO = (MemoryObject)this.getInput("NEXT_STEP");
        allPlanMO = (MemoryObject)this.getOutput("ALL_PLAN");
        selfInfoMO=(MemoryObject)this.getInput("INNER");
    }

    @Override
    public void calculateActivation() {
    }

    @Override
    public void proc() {
        cis = (CreatureInnerSense) selfInfoMO.getI();
        step = (WorldPoint) nextStepMO.getI();
        plan = Collections.synchronizedList((List<WorldPoint>) allPlanMO.getI());
        
        if(step != null){
            double x, y;
            x = step.getX();
            y = step.getY();
            
            double selfX=cis.position.getX();
            double selfY=cis.position.getY();

            Point2D pStep = new Point();
            pStep.setLocation(x, y);

            Point2D pSelf = new Point();
            pSelf.setLocation(selfX, selfY);

            double distance = pSelf.distance(pStep);
            
            try{
                if(distance < reachDistance){ //eat it						
                    //System.out.println("Target");
                    DestroyStep();
                }
            }catch (Exception e) {
               e.printStackTrace();
            }
        }
    }
    
    public void DestroyStep() {
           synchronized(plan) {
               if (plan.size() != 0){
                plan.remove(0);
                step = null;
               }
           }
        }
    
}
