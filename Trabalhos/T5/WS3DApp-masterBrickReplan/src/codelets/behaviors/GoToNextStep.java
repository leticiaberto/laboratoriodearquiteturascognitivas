/*
 * Copyright (C) 2018 leticia.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301  USA
 */
package codelets.behaviors;

import br.unicamp.cst.core.entities.Codelet;
import br.unicamp.cst.core.entities.MemoryObject;
import java.awt.Point;
import java.awt.geom.Point2D;
import memory.CreatureInnerSense;
import org.json.JSONException;
import org.json.JSONObject;
import ws3dproxy.model.WorldPoint;

/**
 *
 * @author leticia
 */
public class GoToNextStep extends Codelet{

    private MemoryObject nextStepMO;
    private MemoryObject allPlanMO;
    private MemoryObject legsMO;
    private MemoryObject selfInfoMO;
    private int creatureBasicSpeed;
    private int reachDistance;
    
    public GoToNextStep(int creatureBasicSpeed, int reachDistance){ 
        this.creatureBasicSpeed=creatureBasicSpeed;
        this.reachDistance = reachDistance;
    }
    @Override
    public void accessMemoryObjects() {
        nextStepMO = (MemoryObject)this.getInput("NEXT_STEP");
        allPlanMO = (MemoryObject)this.getOutput("ALL_PLAN");
        legsMO = (MemoryObject)this.getOutput("LEGS");
        selfInfoMO=(MemoryObject)this.getInput("INNER");
    }

    @Override
    public void calculateActivation() {
    }

    @Override
    public void proc() {
        
        WorldPoint step = (WorldPoint) nextStepMO.getI();
        CreatureInnerSense cis = (CreatureInnerSense) selfInfoMO.getI();
        JSONObject message=new JSONObject();
         double x = 0, y = 0;
        if(step != null){
            //double x, y;
            //System.out.println("next");
            x = step.getX();
            y = step.getY();
            
            double selfX=cis.position.getX();
            double selfY=cis.position.getY();

            Point2D pStep = new Point();
            pStep.setLocation(x, y);

            Point2D pSelf = new Point();
            pSelf.setLocation(selfX, selfY);

            double distance = pSelf.distance(pStep);
            //System.out.println("distancia: " + distance);
            //JSONObject message=new JSONObject();
            try {
                if(distance>reachDistance){ //Go to it
                     //System.out.println("next2");
                        message.put("ACTION", "GOTO");
                        message.put("X", (int)x);
                        message.put("Y", (int)y);
                        message.put("SPEED", creatureBasicSpeed);	

                }else{//Stop
                     //System.out.println("next3");
                        message.put("ACTION", "GOTO");
                        message.put("X", (int)x);
                        message.put("Y", (int)y);
                        message.put("SPEED", 0.0);	
                }
                
                legsMO.setI(message.toString());
                
            } catch (JSONException e) {
                    e.printStackTrace();
            }
           
        }
    }
    
}
