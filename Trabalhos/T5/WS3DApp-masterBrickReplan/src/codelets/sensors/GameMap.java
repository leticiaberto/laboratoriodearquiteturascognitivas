/*
 * Copyright (C) 2018 leticia.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301  USA
 */
package codelets.sensors;

import br.unicamp.cst.core.entities.Codelet;
import br.unicamp.cst.core.entities.MemoryObject;
import java.util.List;
import memory.CreatureInnerSense;
import ws3dproxy.model.Creature;
import ws3dproxy.model.Thing;
import ws3dproxy.model.World;
import ws3dproxy.util.Constants;

/**
 *
 * @author leticia
 */
public class GameMap extends Codelet {
    
    private MemoryObject gridMO;
    private MemoryObject innerMO;
    private Creature c;
    private int [][] matrizCenario = new int[12][16];
    
    public GameMap(Creature nc){
        c = nc;
        for(int i = 0; i < 12; i++){
            for(int j = 0; j < 16; j++){
                matrizCenario[i][j] = 0; 
             }
         }
    }

    @Override
    public void accessMemoryObjects() {
        gridMO = (MemoryObject)this.getOutput("GRID");
        innerMO = (MemoryObject)this.getInput("INNER");
    }

    @Override
    public void calculateActivation() {
    }

    @Override
    public void proc() {
        CreatureInnerSense cis = (CreatureInnerSense) innerMO.getI();

        double x1, x2, y1, y2, posX, posY;
        try{
            List<Thing> worldEntities = World.getWorldEntities();
            for(Thing t: worldEntities)
                if (t.getCategory() == Constants.categoryBRICK){
                    x1 = t.getX1();
                    x2 = t.getX2();
                    y1 = t.getY1();
                    y2 = t.getY2();
                    posX = (x2 - x1) / 50;
                    posY = (y2 - y1) / 50;
                    //System.out.println("X = " + x1 + " Y = "+ y1);
                   // System.out.println("PosX = " + posX + " PosY = "+ posY);
                    int i = (int) y1 / 50;//posição inicial linha 
                    int j = (int) x1 / 50;//posição inicial coluna
                                
                    if(posY > 1.00)//coluna fixa (parede na vertical)
                       for(double k = y1; k < y2; k = k + 50){
                           matrizCenario[i][j] = 1;
                           i++;
                       }
                    else//linha fica (parede na horizontal)
                        for(double k = x1; k < x2; k = k + 50){
                            matrizCenario[i][j] = 1; 
                            j++;
                        }    
                }
            //posição da criatura na matriz
            double xC, yC;
            xC = cis.position.getX();
            yC = cis.position.getY();
            if (xC/50 <= 12 && yC/50 <=15)
                matrizCenario[(int) xC/50][(int) yC/50] = 2;
            
            /*System.out.println("GAME MAP"); 
            for(int i = 0; i < 12; i++){
                for(int j = 0; j < 16; j++){
                    System.out.print(matrizCenario[i][j] + " "); 
                }
                System.out.println(); 
            }*/
            gridMO.setI(matrizCenario);
        }catch (Exception e) {
            e.printStackTrace();
        }
        //gridMO.setI();
    }
    
}
