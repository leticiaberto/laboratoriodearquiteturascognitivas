package modules;

import edu.memphis.ccrg.lida.environment.EnvironmentImpl;
import edu.memphis.ccrg.lida.framework.tasks.FrameworkTaskImpl;
import edu.uci.ics.jung.graph.Graph;
import edu.uci.ics.jung.graph.UndirectedGraph;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import ws3dproxy.WS3DProxy;
import ws3dproxy.model.Creature;
import ws3dproxy.model.Leaflet;
import ws3dproxy.model.Thing;
import ws3dproxy.model.World;
import ws3dproxy.util.Constants;

public class Environment extends EnvironmentImpl {

    private static final int DEFAULT_TICKS_PER_RUN = 100;
    private int ticksPerRun;
    private WS3DProxy proxy;
    private Creature creature;
    private Thing food;
    private Thing jewel;
    private List<Thing> thingAhead;
    private Thing leafletJewel;
    private String currentAction;   
    private Thing brick;
    
    private double xDest = 800;
    private double yDest = 600;
    private int [][] matrizCenario = new int[12][16];
    
   // private static final int ROWS = 192;
    //private static final int COLS = 192;
    //private int [][]Grafo;
    
       
    public Environment() {
        this.ticksPerRun = DEFAULT_TICKS_PER_RUN;
        this.proxy = new WS3DProxy();
        this.creature = null;
        this.food = null;
        this.jewel = null;
        this.thingAhead = new ArrayList<>();
        this.brick = null;
        this.leafletJewel = null;
        this.currentAction = "rotate";
        //this.Grafo = new int [ROWS][COLS];
    }

    @Override
    public void init() {
        super.init();
        ticksPerRun = (Integer) getParam("environment.ticksPerRun", DEFAULT_TICKS_PER_RUN);
        taskSpawner.addTask(new BackgroundTask(ticksPerRun));
        
        try {
            System.out.println("Reseting the WS3D World ...");
            proxy.getWorld().reset();
            creature = proxy.createCreature(100, 100, 0);
            creature.start();
            System.out.println("Starting the WS3D Resource Generator ... ");
            //World.grow(1);
            /*maxWidth = 800
                maxHeigth = 600 */
            World.createBrick(2, 0, 0, 750, 50);//parede de cima
            World.createBrick(2, 50, 150, 500, 200);//parede de baixo
           // World.createBrick(2, 50, 200, 500, 250);
            World.createBrick(2, 500, 150, 548,500);//lateral esquerda
            //World.createBrick(2, 600, 150, 650,500);//lateral direita
            //World.createBrick(2, 650, 150, 700, 200);//baixo - sem saida
            World.createBrick(2, 700, 50, 750, 500);
            World.createBrick(2, 50, 450, 450, 500);
            World.createBrick(2, 50, 500, 100, 600);
            World.createBrick(2, 300, 300, 350,500);
            World.createBrick(2, 100, 200, 150,350);
            World.createBrick(2, 0, 300, 50, 350);
            Thread.sleep(4000);
            creature.updateState();
            
            //Mapeamento de paredes
            //espa�os andaveis = 0; nao andaveis (paredes) = 1
            //int [][] matrizCenario = new int[12][16];
            for(int i = 0; i < 12; i++){
               for(int j = 0; j < 16; j++){
                   matrizCenario[i][j] = 0; 
                }
            }
            
            double x1, x2, y1, y2, posX, posY;
            List<Thing> worldEntities = World.getWorldEntities();
            for(Thing t: worldEntities)
                if (t.getCategory() == Constants.categoryBRICK){
                    x1 = t.getX1();
                    x2 = t.getX2();
                    y1 = t.getY1();
                    y2 = t.getY2();
                    posX = (x2 - x1) / 50;
                    posY = (y2 - y1) / 50;
                    //System.out.println("X = " + x1 + " Y = "+ y1);
                   // System.out.println("PosX = " + posX + " PosY = "+ posY);
                    int i = (int) y1 / 50;//posi��o inicial linha 
                    int j = (int) x1 / 50;//posi��o inicial coluna
                                
                    if(posY > 1.00)//coluna fixa (parede na vertical)
                       for(double k = y1; k < y2; k = k + 50){
                           matrizCenario[i][j] = 1;
                           i++;
                       }
                    else//linha fica (parede na horizontal)
                        for(double k = x1; k < x2; k = k + 50){
                            matrizCenario[i][j] = 1; 
                            j++;
                        }    
                }
            //posi��o da criatura na matriz
            double xC, yC;
            xC = creature.getPosition().getX();
            yC = creature.getPosition().getY();
            matrizCenario[(int) xC/50][(int) yC/50] = 2;
            
            /*for(int i = 0; i < 12; i++){
               for(int j = 0; j < 16; j++){
                  System.out.print(matrizCenario[i][j] + " "); 
                }
               System.out.println("");
            }*/
            
            //gerando o grafo
            /*for(int i = 0; i < 192; i++){
               for(int j = 0; j < 192; j++){
                   Grafo[i][j] = 0;//pode ir para todos os lugares
               }
            }
            
            //Matriz de adjacencias
            for(int i = 0; i < 12; i++){
               for(int j = 0; j < 15; j++){//pq ja chega na ultima coluna
                   if(matrizCenario[i][j] == 0 && matrizCenario[i][j + 1] == 0)
                    Grafo[i][j] = 1;
               }
            }
            
            for(int j = 0; j < 16; j++){
               for(int i = 0; i < 11; i++){//pq ja chega na ultima linha
                   if(matrizCenario[i][j] == 0 && matrizCenario[i + 1][j] == 0)
                    Grafo[i][j] = 1;//nao tem liga��o entre as celulas - tem parede
               }
            }*/
            
           
            
            System.out.println("DemoLIDA has started...");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private class BackgroundTask extends FrameworkTaskImpl {

        public BackgroundTask(int ticksPerRun) {
            super(ticksPerRun);
        }

        @Override
        protected void runThisFrameworkTask() {
            updateEnvironment();
            performAction(currentAction);
        }
    }

    @Override
    public void resetState() {
        currentAction = "rotate";
    }

    @Override
    public Object getState(Map<String, ?> params) {
        Object requestedObject = null;
        String mode = (String) params.get("mode");
        switch (mode) {
            case "food":
                requestedObject = food;
                break;
            case "jewel":
                requestedObject = jewel;
                break;
            case "thingAhead":
                requestedObject = thingAhead;
                break;
            case "leafletJewel":
                requestedObject = leafletJewel;
                break;
            case "brick":
                requestedObject = brick;
                break;
            default:
                break;
        }
        return requestedObject;
    }

    
    public void updateEnvironment() {
        creature.updateState();
        food = null;
        jewel = null;
        leafletJewel = null;
        thingAhead.clear();
        brick = null;
                
        for (Thing thing : creature.getThingsInVision()) {
            if (creature.calculateDistanceTo(thing) <= Constants.OFFSET) {
                // Identifica o objeto proximo
                thingAhead.add(thing);
                break;
            } else if (thing.getCategory() == Constants.categoryJEWEL) {
                if (leafletJewel == null) {
                    // Identifica se a joia esta no leaflet
                    for(Leaflet leaflet: creature.getLeaflets()){
                        if (leaflet.ifInLeaflet(thing.getMaterial().getColorName()) &&
                                leaflet.getTotalNumberOfType(thing.getMaterial().getColorName()) > leaflet.getCollectedNumberOfType(thing.getMaterial().getColorName())){
                            leafletJewel = thing;
                            break;
                        }
                    }
                } else {
                    // Identifica a joia que nao esta no leaflet
                    jewel = thing;
                }
            } else if (food == null && creature.getFuel() <= 300.0
                        && (thing.getCategory() == Constants.categoryFOOD
                        || thing.getCategory() == Constants.categoryPFOOD
                        || thing.getCategory() == Constants.categoryNPFOOD)) {
                
                    // Identifica qualquer tipo de comida
                    food = thing;
            } else if (thing.getCategory() == Constants.categoryBRICK){
                brick = thing;
            }   
        }
    }
    
    
    
    @Override
    public void processAction(Object action) {
        String actionName = (String) action;
        currentAction = actionName.substring(actionName.indexOf(".") + 1);
    }

    public String getAction(){
        return currentAction;
    }
    
    private void performAction(String currentAction) {
        try {
            //System.out.println("Action: "+currentAction);
            switch (currentAction) {
                case "rotate":
                    creature.rotate(1.0);
                    //CommandUtility.sendSetTurn(creature.getIndex(), -1.0, -1.0, 3.0);
                    break;
                case "gotoFood":
                    if (food != null) 
                        creature.moveto(3.0, food.getX1(), food.getY1());
                        //CommandUtility.sendGoTo(creature.getIndex(), 3.0, 3.0, food.getX1(), food.getY1());
                    break;
                case "gotoJewel":
                    if (leafletJewel != null)
                        creature.moveto(3.0, leafletJewel.getX1(), leafletJewel.getY1());
                        //CommandUtility.sendGoTo(creature.getIndex(), 3.0, 3.0, leafletJewel.getX1(), leafletJewel.getY1());
                    break;                    
                case "get":
                    creature.move(0.0, 0.0, 0.0);
                    //CommandUtility.sendSetTurn(creature.getIndex(), 0.0, 0.0, 0.0);
                    if (thingAhead != null) {
                        for (Thing thing : thingAhead) {
                            if (thing.getCategory() == Constants.categoryJEWEL) {
                                creature.putInSack(thing.getName());
                            } else if (thing.getCategory() == Constants.categoryFOOD || thing.getCategory() == Constants.categoryNPFOOD || thing.getCategory() == Constants.categoryPFOOD) {
                                creature.eatIt(thing.getName());
                            }
                        }
                    }
                    this.resetState();
                    break;
                case "avoidBrick":
                    double xC, yC;
                    xC = creature.getPosition().getX();
                    yC = creature.getPosition().getY();
                    
                    //System.out.println("X: " + xC + " Y: " + yC);
                    int i = 0;
                    int j = 0;
                    double [] dist = new double [4];
                    double min = 0;
                    double minCal = 0, empateI = 0, empateJ = 0, empate = 999999;
                    //int flagI = 1, flagJ = 1;
                     for (i = 0; i <4; i++)
                         dist[i] = 1000000;
                    
                     int minI = 0, minJ = 0;
                    for(i = 0; i < 12; i++){
                        for(j = 0; j < 16; j++){
                            if(matrizCenario[i][j] == 2){
                                minI = i;
                                minJ = j;
                                break;
                            }
                        }
                    }
                   
                    i = minI;
                    j = minJ;
                  
                    if(i == 11 && j == 15)
                        creature.rotate(1.0);
                    else{
                        minI = i;
                        minJ = j+1;
                       // plano[l] = "+y";
                       if(j+1 < 16){
                        if(matrizCenario[i][j+1] == 0)
                            min = Math.sqrt(Math.pow((xDest - xC),2) + Math.pow((yDest - yC + 50),2));
                       }
                        //System.out.println("min: " + min);
                       //System.out.println(min);
                       if(j-1 >= 0){
                        if(matrizCenario[i][j-1] == 0){
                            minCal = Math.sqrt(Math.pow((xDest - xC),2) + Math.pow((yDest - yC - 50),2));
                             if (minCal > min){
                                 min = minCal;
                                 minI = i;
                                 minJ = j-1;
                                
                             }
                             if (minCal == min){
                                 empateI = i;
                                 empateJ = j -1;
                                 empate = min;
                                 
                                 //System.out.println("Detectou empate!");
                             }
                             //System.out.println("min: " + minCal);
                        }
                        
                       }
                       //System.out.println(minCal);
                       if(i+1 < 12){
                        if(matrizCenario[i+1][j] == 0){
                            minCal = Math.sqrt(Math.pow((xDest - xC + 50),2) + Math.pow((yDest - yC),2));
                            if (minCal > min){
                                 min = minCal;
                                 minI = i + 1;
                                 minJ = j;
                                 //System.out.println("AQUI: " + min);
                                 //plano[l] = "+x";
                             }
                             if (minCal == min){
                                 empateI = i + 1;
                                 empateJ = j;
                                 empate = min;
                                 //flagI = 0;
                                 //System.out.println("Detectou empate!");
                             }
                             //System.out.println("min: " + minCal);
                        }
                       }
                       if (i-1 >= 0){
                        if(matrizCenario[i-1][j] == 0){
                           minCal = Math.sqrt(Math.pow((xDest - xC - 50),2) + Math.pow((yDest - yC),2));
                            if (minCal > min){
                                min = minCal;
                                minI = i - 1;
                                minJ = j;//plano[l] = "-x";
                            }
                            if (minCal == min){
                                empateI = i -1;
                                empateJ = j;
                                empate = min;
                                //flagI = 0;
                                //System.out.println("Detectou empate!");
                            }
                            //System.out.println("min: " + minCal);
                        }
                       }
                        //int minEmpate = 2;
                        
                        /*int auxMinEmpateI = (int)empateI;
                        int auxMinEmpateJ = (int)empateJ;
                        int auxMinI = minI;
                        int auxMinJ = minJ;*/
                        
                        /*if (min == empate){  
                            System.out.println("entrou while");
                            while(minI < 12 && minJ < 16 && empateI < 12 && empateJ < 16){
                                if (matrizCenario[minI][minJ] == 0){
                                    minEmpate = 1;  
                                    System.out.println("entrou minEmpate 1");
                                }
                                else 
                                    break;
                                 if (matrizCenario[(int)empateI][(int)empateJ] == 0){
                                    minEmpate = 2;
                                   System.out.println("entrou minEmpate 2");
                                }
                                 else
                                    break;
                                 if(flagI == 1){
                                     minI++;
                                     empateI++;
                                 }
                                 else{
                                     minI--;
                                     empateI--;
                                 }
                                 if(flagJ == 1){
                                     minJ++;
                                     empateJ++;
                                 }
                                 else{
                                     minJ--;
                                     empateJ--;
                                 }
                            }
                            if(minEmpate == 1){
                                minI = auxMinI;
                                minJ = auxMinJ;
                            }
                            else{
                                minI = auxMinEmpateI;
                                minJ = auxMinEmpateJ;
                            }
                        }*/

                        matrizCenario[i][j] = 0;
                        //System.out.println("minI: " + minI + " minJ: " + minJ);
                        matrizCenario[minI][minJ] = 2;
                                         
                        
                        double x = 0, y = 0;
                        if(minI == i)
                            y = yC;
                        else if(minI == i + 1){
                            y = yC + 50.00;
                            if(y % 50.00 != 0)
                                y = y + (50.00 - y % 50.00);
                            //plano[l] = "+y";
                            //l++;
                        }
                        else if(minI == i - 1){
                                y = yC - 50.00;
                                if(y % 50.00 != 0)
                                    y = y - (50.00 + y % 50.00);
                        }
                           // plano[l] = "-y";
                            //l++;
                        

                        if(minJ == j)
                            x = xC;
                        else if(minJ == j + 1){
                            x = xC + 50.00;
                            //System.out.println("x antes mod: " + x);
                            if(x % 50.00 != 0)
                                x = x + (50.00 - x % 50.00);
                            //System.out.println("x mod: " + x);
                            //plano[l] = "+x";
                           // l++;
                        }
                        else if(minJ == j - 1){
                            x = xC - 50.00;
                             if(x % 50.00 != 0)
                                x = x - (50.00 - x % 50.00);
                           // plano[l] = "-x";
                            //l++;
                        }
                        /*System.out.println("l: " + l);
                        l = 0;
                        if(matrizCenario[11][15] == 2){
                            
                            while(!plano[l].equals("")){
                                System.out.println("entrou");
                                if(plano[l].equals("+y")){
                                    creature.moveto(3.0, xC, yC + 50);
                                    yC = yC + 50;
                                }
                                if(plano[l].equals("-y")){
                                    creature.moveto(3.0, xC, yC - 50);
                                    yC = yC - 50;
                                }
                                if(plano[l].equals("+x")){
                                    creature.moveto(3.0, xC + 50, yC);
                                    xC = xC + 50;
                                }
                                if(plano[l].equals("-x")){
                                    creature.moveto(3.0, xC - 50, yC);
                                    xC = xC - 50;
                                }
                                l++;
                            }
                        }*/
                         //System.out.println("antes");
                        //creature.updateState();
                       // System.out.println("X: " + xC + " Y: " + yC);
                        //System.out.println("Xcal: " + x + " Ycal: " + y);
                        
                        int delta = 1;
                       // double auxX = xC;
                        //double auxY = yC;
                        while ((x - xC) > delta || (y - yC) > delta){
                           creature.moveto(1.0, x, y);
                          //System.out.println("Xm: " + xC + " Ym: " + yC);
                           creature.updateState();
                           xC = creature.getPosition().getX();
                           yC = creature.getPosition().getY();
                           creature.move(0.0, 0.0, 0.0);
                        }
                        /*if(xC != auxX){
                            xC = xC - x;
                            creature.moveto(3.0, xC, yC);
                        }
                        if(yC != auxY){
                            yC = yC + y;
                            creature.moveto(3.0, xC, yC);
                        }*/
                       //creature.move(0.0, 0.0, 0.0);
                        //Thread.sleep(1);
                        /*for( i = 0; i < 12; i++){
                            for( j = 0; j < 16; j++){
                               System.out.print(matrizCenario[i][j] + " "); 
                             }
                            System.out.println("");
                        }*/
                        
                    }
                    break;
                default:
                    break;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
