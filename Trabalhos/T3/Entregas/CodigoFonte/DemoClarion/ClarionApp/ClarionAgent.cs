﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Globalization;
using Clarion;
using Clarion.Framework;
using Clarion.Framework.Core;
using Clarion.Framework.Templates;
using ClarionApp.Model;
using ClarionApp;
using System.Threading;
using Gtk;

namespace ClarionApp
{
    /// <summary>
    /// Public enum that represents all possibilities of agent actions
    /// </summary>
    public enum CreatureActions
    {
        DO_NOTHING,
        ROTATE_CLOCKWISE,
        GO_AHEAD,
		MOVE_JEWEL, //Leticia
		GET_JEWEL, //Leticia
		EAT_FOOD //Leticia
    }

    public class ClarionAgent
    {
        #region Constants

		public List<Thing> knapsack = new List<Thing>(); //Leticia

        /// <summary>
        /// Constant that represents the Visual Sensor
        /// </summary>
        private String SENSOR_VISUAL_DIMENSION = "VisualSensor";
        /// <summary>
        /// Constant that represents that there is at least one wall ahead
        /// </summary>
        private String DIMENSION_WALL_AHEAD = "WallAhead";

		//Leticia
		/// <summary>
		/// Constant that represents that there is at least one jewel close
		/// </summary>
		private String DIMENSION_JEWEL_CLOSE = "JewelClose";
		private String DIMENSION_JEWEL_SACK = "JewelSack";
		private String DIMENSION_FOOD_EAT = "FoodEat";
		private String DIMENSION_DONE = "Done";

		double prad = 0;
        #endregion

        #region Properties
		public MindViewer mind;
		String creatureId = String.Empty;
		String creatureName = String.Empty;
        #region Simulation
        /// <summary>
        /// If this value is greater than zero, the agent will have a finite number of cognitive cycle. Otherwise, it will have infinite cycles.
        /// </summary>
        public double MaxNumberOfCognitiveCycles = -1;
        /// <summary>
        /// Current cognitive cycle number
        /// </summary>
        private double CurrentCognitiveCycle = 0;
        /// <summary>
        /// Time between cognitive cycle in miliseconds
        /// </summary>
        public Int32 TimeBetweenCognitiveCycles = 0;
        /// <summary>
        /// A thread Class that will handle the simulation process
        /// </summary>
        private Thread runThread;
        #endregion

        #region Agent
		private WSProxy worldServer;
        /// <summary>
        /// The agent 
        /// </summary>
        private Clarion.Framework.Agent CurrentAgent;
        #endregion

        #region Perception Input
        /// <summary>
        /// Perception input to indicates a wall ahead
        /// </summary>
		private DimensionValuePair inputWallAhead;

		//Leticia
		/// <summary>
		/// Perception input to indicates a jewel
		/// </summary>
		private DimensionValuePair InputJewelClose;
		private DimensionValuePair InputJewelSack;
		private DimensionValuePair InputFoodEat;
		private DimensionValuePair InputDone;

        #endregion

        #region Action Output
        /// <summary>
        /// Output action that makes the agent to rotate clockwise
        /// </summary>
		private ExternalActionChunk outputRotateClockwise;
		private ExternalActionChunk outputGoAhead;
        /// <summary>
        /// Output action that makes the agent go ahead
        /// </summary>
		/// Leticia
		private ExternalActionChunk outputGoToJewel;
		private ExternalActionChunk outputSackJewel;
		private ExternalActionChunk outputEatFood;
		private ExternalActionChunk outputDone;

        #endregion

        #endregion

		//Leticia
		public IList<Thing> CurrentSensorialInfo; //para poder usar no processAction
        
		#region Constructor
		public ClarionAgent(WSProxy nws, String creature_ID, String creature_Name)
        {
			worldServer = nws;
			// Initialize the agent
            CurrentAgent = World.NewAgent("Current Agent");
			mind = new MindViewer();
			mind.Show ();
			creatureId = creature_ID;
			creatureName = creature_Name;

            // Initialize Input Information
            inputWallAhead = World.NewDimensionValuePair(SENSOR_VISUAL_DIMENSION, DIMENSION_WALL_AHEAD);

			//Leticia
			InputJewelClose = World.NewDimensionValuePair(SENSOR_VISUAL_DIMENSION, DIMENSION_JEWEL_CLOSE);
			InputJewelSack = World.NewDimensionValuePair(SENSOR_VISUAL_DIMENSION, DIMENSION_JEWEL_SACK);
			InputFoodEat = World.NewDimensionValuePair(SENSOR_VISUAL_DIMENSION, DIMENSION_FOOD_EAT);
			InputDone = World.NewDimensionValuePair(SENSOR_VISUAL_DIMENSION, DIMENSION_DONE);


            // Initialize Output actions
            outputRotateClockwise = World.NewExternalActionChunk(CreatureActions.ROTATE_CLOCKWISE.ToString());
            outputGoAhead = World.NewExternalActionChunk(CreatureActions.GO_AHEAD.ToString());

			//Leticia 
			outputGoToJewel = World.NewExternalActionChunk(CreatureActions.MOVE_JEWEL.ToString());
			outputSackJewel = World.NewExternalActionChunk(CreatureActions.GET_JEWEL.ToString());
			outputEatFood = World.NewExternalActionChunk(CreatureActions.EAT_FOOD.ToString ());
			outputDone = World.NewExternalActionChunk (CreatureActions.DO_NOTHING.ToString ());

            //Create thread to simulation
            runThread = new Thread(CognitiveCycle);
			Console.WriteLine("Agent started");
        }
        #endregion

        #region Public Methods
        /// <summary>
        /// Run the Simulation in World Server 3d Environment
        /// </summary>
        public void Run()
        {                
			Console.WriteLine ("Running ...");
            // Setup Agent to run
            if (runThread != null && !runThread.IsAlive)
            {
                SetupAgentInfraStructure();
				// Start Simulation Thread                
                runThread.Start(null);
            }
        }

        /// <summary>
        /// Abort the current Simulation
        /// </summary>
        /// <param name="deleteAgent">If true beyond abort the current simulation it will die the agent.</param>
        public void Abort(Boolean deleteAgent)
        {   Console.WriteLine ("Aborting ...");
            if (runThread != null && runThread.IsAlive)
            {
                runThread.Abort();
            }

            if (CurrentAgent != null && deleteAgent)
            {
                CurrentAgent.Die();
            }
        }

		IList<Thing> processSensoryInformation()
		{
			IList<Thing> response = null;

			if (worldServer != null && worldServer.IsConnected)
			{
				response = worldServer.SendGetCreatureState(creatureName);
				prad = (Math.PI / 180) * response.First().Pitch;
				while (prad > Math.PI) prad -= 2 * Math.PI;
				while (prad < - Math.PI) prad += 2 * Math.PI;
				Sack s = worldServer.SendGetSack("0");
				mind.setBag(s);
			}

			return response;
		}

		void processSelectedAction(CreatureActions externalAction)
		{   Thread.CurrentThread.CurrentCulture = new CultureInfo("en-US");
			if (worldServer != null && worldServer.IsConnected)
			{
				switch (externalAction)
				{
				case CreatureActions.DO_NOTHING:
					worldServer.SendSetAngle (creatureId, 0, 0, 0);
					// Do nothing as the own value says
					break;
				case CreatureActions.ROTATE_CLOCKWISE:
					worldServer.SendSetAngle(creatureId, 2, -2, 2);
					break;
				case CreatureActions.GO_AHEAD:
					worldServer.SendSetAngle(creatureId, 1, 1, prad);
					break;
				//Leticia
				case CreatureActions.GET_JEWEL:
					Thing sackJewel = CurrentSensorialInfo.Where(item => (item.CategoryId == Thing.CATEGORY_JEWEL)).OrderBy(item => item.DistanceToCreature).First ();
					worldServer.SendSackIt(creatureId, sackJewel.Name);
					bool already = false;
					foreach(Thing tk in knapsack){
						if (tk.Name.CompareTo(sackJewel.Name) == 0) {
							already = true;
							break;
						}
					}
					if(!already)
						knapsack.Add(sackJewel);
					break;
				case CreatureActions.EAT_FOOD:
					Thing food = CurrentSensorialInfo.Where(item => (item.CategoryId == Thing.CATEGORY_NPFOOD || item.CategoryId == Thing.categoryPFOOD)).OrderBy(item => item.DistanceToCreature).First ();
					worldServer.SendEatIt(creatureId, food.Name);
					break;
				case CreatureActions.MOVE_JEWEL:
					Thing closestJewel = null;
					Thing closestLeafletJewel = null;

					Creature c = (Creature) CurrentSensorialInfo.Where(item => (item.CategoryId == Thing.CATEGORY_CREATURE)).First();

					if(CurrentSensorialInfo != null){
						foreach(Thing t in CurrentSensorialInfo){
							if(t.CategoryId == Thing.CATEGORY_JEWEL) {
								if(closestJewel != null && closestJewel.DistanceToCreature > t.DistanceToCreature) {
									closestJewel = t;
								}
								else if(closestJewel == null){
									closestJewel = t;
								}

								if(closestLeafletJewel == null && c.IsLeafletMissingJewel(t.Material.Color)){
									closestLeafletJewel = t;
								} 
								else if(closestLeafletJewel != null && c.IsLeafletMissingJewel(t.Material.Color)){
									if(closestLeafletJewel.DistanceToCreature > t.DistanceToCreature){
										closestLeafletJewel = t;
									}
								}
							}
						}

						if (closestLeafletJewel != null) {
							closestJewel = closestLeafletJewel;
						}

						double x = (closestJewel.X1 + closestJewel.X2) / 2.0f;
						double y = (closestJewel.Y1 + closestJewel.Y2) / 2.0f;
						worldServer.SendSetGoTo(creatureId, 1.5f, 1.5f, x, y);
					}
					break;
				default:
					break;
				}
			}
		}

        #endregion

        #region Setup Agent Methods
        /// <summary>
        /// Setup agent infra structure (ACS, NACS, MS and MCS)
        /// </summary>
        private void SetupAgentInfraStructure()
        {
            // Setup the ACS Subsystem
            SetupACS();                    
        }

        private void SetupMS()
        {            
            //RichDrive
        }

        /// <summary>
        /// Setup the ACS subsystem
        /// </summary>
        private void SetupACS()
        {
            // Create Rule to avoid collision with wall
            SupportCalculator avoidCollisionWallSupportCalculator = FixedRuleToAvoidCollisionWall;
            FixedRule ruleAvoidCollisionWall = AgentInitializer.InitializeActionRule(CurrentAgent, FixedRule.Factory, outputRotateClockwise, avoidCollisionWallSupportCalculator);
			// Commit this rule to Agent (in the ACS)
			CurrentAgent.Commit(ruleAvoidCollisionWall);

			// Create Colission To Go Ahead
			SupportCalculator goAheadSupportCalculator = FixedRuleToGoAhead;
			FixedRule ruleGoAhead = AgentInitializer.InitializeActionRule(CurrentAgent, FixedRule.Factory, outputGoAhead, goAheadSupportCalculator);
			// Commit this rule to Agent (in the ACS)
			CurrentAgent.Commit(ruleGoAhead);

			//Leticia
			SupportCalculator lookForJewelSupportCalculator = FixedRuleDelegateToLookForJewel;
			FixedRule ruleLookForJewel = AgentInitializer.InitializeActionRule(CurrentAgent, FixedRule.Factory, outputRotateClockwise, lookForJewelSupportCalculator);
			CurrentAgent.Commit(ruleLookForJewel);

			SupportCalculator goToJewelSupportCalculator = FixedRuleDelegateToGoToJewel;
			FixedRule ruleToGoToJewel = AgentInitializer.InitializeActionRule(CurrentAgent, FixedRule.Factory, outputGoToJewel, goToJewelSupportCalculator);
			CurrentAgent.Commit(ruleToGoToJewel);

			SupportCalculator sackJewelSupportCalculator = FixedRuleDelegateToSackJewel;
			FixedRule ruleToSackJewel = AgentInitializer.InitializeActionRule(CurrentAgent, FixedRule.Factory, outputSackJewel, sackJewelSupportCalculator);
			CurrentAgent.Commit(ruleToSackJewel);

			SupportCalculator eatFoodSupportCalculator = FixedRuleDelegateToEatFood;
			FixedRule ruleToEatFood = AgentInitializer.InitializeActionRule(CurrentAgent, FixedRule.Factory, outputEatFood, eatFoodSupportCalculator);
			CurrentAgent.Commit(ruleToEatFood);

			SupportCalculator doneSupportCalculator = FixedRuleDelegateDone;
			FixedRule ruleDone = AgentInitializer.InitializeActionRule (CurrentAgent, FixedRule.Factory, outputDone, doneSupportCalculator);
			CurrentAgent.Commit (ruleDone);


            // Disable Rule Refinement
            CurrentAgent.ACS.Parameters.PERFORM_RER_REFINEMENT = false;

            // The selection type will be probabilistic
            CurrentAgent.ACS.Parameters.LEVEL_SELECTION_METHOD = ActionCenteredSubsystem.LevelSelectionMethods.STOCHASTIC;

            // The action selection will be fixed (not variable) i.e. only the statement defined above.
            CurrentAgent.ACS.Parameters.LEVEL_SELECTION_OPTION = ActionCenteredSubsystem.LevelSelectionOptions.FIXED;

            // Define Probabilistic values
            CurrentAgent.ACS.Parameters.FIXED_FR_LEVEL_SELECTION_MEASURE = 1;
            CurrentAgent.ACS.Parameters.FIXED_IRL_LEVEL_SELECTION_MEASURE = 0;
            CurrentAgent.ACS.Parameters.FIXED_BL_LEVEL_SELECTION_MEASURE = 0;
            CurrentAgent.ACS.Parameters.FIXED_RER_LEVEL_SELECTION_MEASURE = 0;
        }

        /// <summary>
        /// Make the agent perception. In other words, translate the information that came from sensors to a new type that the agent can understand
        /// </summary>
        /// <param name="sensorialInformation">The information that came from server</param>
        /// <returns>The perceived information</returns>
		private SensoryInformation prepareSensoryInformation(IList<Thing> listOfThings)
        {
            // New sensory information
            SensoryInformation si = World.NewSensoryInformation(CurrentAgent);

            // Detect if we have a wall ahead
            Boolean wallAhead = listOfThings.Where(item => (item.CategoryId == Thing.CATEGORY_BRICK && item.DistanceToCreature <= 61)).Any();
            double wallAheadActivationValue = wallAhead ? CurrentAgent.Parameters.MAX_ACTIVATION : CurrentAgent.Parameters.MIN_ACTIVATION;
            
			//Leticia
			Boolean hasJewel = listOfThings.Where(item => (item.CategoryId == Thing.CATEGORY_JEWEL)).Any();
			double hasJewelActivationValue = hasJewel ? CurrentAgent.Parameters.MAX_ACTIVATION : CurrentAgent.Parameters.MIN_ACTIVATION;

			Boolean hasFood = listOfThings.Where(item => (item.CategoryId == Thing.categoryPFOOD || item.CategoryId == Thing.CATEGORY_NPFOOD)).Any();

			Thing closestFood = null, closestJewel = null;
			double eatFoodActivationValue = CurrentAgent.Parameters.MIN_ACTIVATION;
			double sackJewelActivationValue = CurrentAgent.Parameters.MIN_ACTIVATION;

			if(hasFood){
				closestFood = listOfThings.Where(item => (item.CategoryId == Thing.categoryPFOOD || item.CategoryId == Thing.CATEGORY_NPFOOD)).OrderBy(item => item.DistanceToCreature).First();
				if(closestFood.DistanceToCreature <= 40){
					eatFoodActivationValue = CurrentAgent.Parameters.MAX_ACTIVATION;
				}
			}

			if(hasJewel){
				closestJewel = listOfThings.Where(item => (item.CategoryId == Thing.CATEGORY_JEWEL)).OrderBy(item => item.DistanceToCreature).First();
				if(closestJewel.DistanceToCreature <= 30){
					sackJewelActivationValue = CurrentAgent.Parameters.MAX_ACTIVATION;
				}
			}

			if(eatFoodActivationValue == CurrentAgent.Parameters.MAX_ACTIVATION 
				&& sackJewelActivationValue == CurrentAgent.Parameters.MAX_ACTIVATION){
				//Both Jewel and Food are close.. get the closest one!
				if(closestJewel.DistanceToCreature <= closestFood.DistanceToCreature){
					eatFoodActivationValue *= 0.5f;
				}
				else{
					sackJewelActivationValue *= 0.5f;//nao tenho certeza dessa ordem
				}
				hasJewelActivationValue *= 0.5f;
			}

			si.Add(InputFoodEat, eatFoodActivationValue);
			si.Add(InputJewelSack, sackJewelActivationValue);
			si.Add(InputJewelClose, hasJewelActivationValue);

			//continua igual o base
			si.Add(inputWallAhead, wallAheadActivationValue);
			//Console.WriteLine(sensorialInformation);
			Creature c = (Creature) listOfThings.Where(item => (item.CategoryId == Thing.CATEGORY_CREATURE)).First();

			//Leticia
			double doneActivationValue = c.isLeafletCompleted() ? CurrentAgent.Parameters.MAX_ACTIVATION : CurrentAgent.Parameters.MIN_ACTIVATION;
			si.Add(InputDone, doneActivationValue);

			int n = 0;
			foreach(Leaflet l in c.getLeaflets()) {
				mind.updateLeaflet(n,l);
				n++;
			}
            return si;
        }
        #endregion

        #region Fixed Rules
        private double FixedRuleToAvoidCollisionWall(ActivationCollection currentInput, Rule target)
        {
            // See partial match threshold to verify what are the rules available for action selection
            return ((currentInput.Contains(inputWallAhead, CurrentAgent.Parameters.MAX_ACTIVATION))) ? 1.0 : 0.0;
        }

        private double FixedRuleToGoAhead(ActivationCollection currentInput, Rule target)
        {
            // Here we will make the logic to go ahead
            return ((currentInput.Contains(inputWallAhead, CurrentAgent.Parameters.MIN_ACTIVATION))) ? 1.0 : 0.0;
        }

		//Leticia
		private double FixedRuleDelegateToLookForJewel(ActivationCollection currentInput, Rule target)
		{
			if (currentInput.Contains(InputDone, CurrentAgent.Parameters.MAX_ACTIVATION)) {
				return 0.0;
			} else 
				return((currentInput.Contains(InputJewelClose, CurrentAgent.Parameters.MIN_ACTIVATION))) ? 1.0 : 0.0;
		}

		private double FixedRuleDelegateToGoToJewel(ActivationCollection currentInput, Rule target)
		{
			return((currentInput.Contains(InputJewelClose, CurrentAgent.Parameters.MAX_ACTIVATION))) ? 1.0 : 0.0;
		}

		private double FixedRuleDelegateToSackJewel(ActivationCollection currentInput, Rule target)
		{
			return((currentInput.Contains(InputJewelSack, CurrentAgent.Parameters.MAX_ACTIVATION))) ? 1.0 : 0.0;
		}

		private double FixedRuleDelegateToEatFood(ActivationCollection currentInput, Rule target)
		{
			return((currentInput.Contains(InputFoodEat, CurrentAgent.Parameters.MAX_ACTIVATION))) ? 1.0 : 0.0;
		}

		private double FixedRuleDelegateDone(ActivationCollection currentInput, Rule target) {
			return((currentInput.Contains(InputDone, CurrentAgent.Parameters.MAX_ACTIVATION))) ? 1.0 : 0.0;
		}

        #endregion

        #region Run Thread Method
        private void CognitiveCycle(object obj)
        {

			Console.WriteLine("Starting Cognitive Cycle ... press CTRL-C to finish !");
            // Cognitive Cycle starts here getting sensorial information
            while (CurrentCognitiveCycle != MaxNumberOfCognitiveCycles)
            {   
				// Get current sensory information                    
				IList<Thing> currentSceneInWS3D = processSensoryInformation();

				CurrentSensorialInfo = currentSceneInWS3D;//Leticia

                // Make the perception
                SensoryInformation si = prepareSensoryInformation(currentSceneInWS3D);

                //Perceive the sensory information
                CurrentAgent.Perceive(si);

                //Choose an action
                ExternalActionChunk chosen = CurrentAgent.GetChosenExternalAction(si);

                // Get the selected action
                String actionLabel = chosen.LabelAsIComparable.ToString();
                CreatureActions actionType = (CreatureActions)Enum.Parse(typeof(CreatureActions), actionLabel, true);

                // Call the output event handler
				processSelectedAction(actionType);

                // Increment the number of cognitive cycles
                CurrentCognitiveCycle++;

                //Wait to the agent accomplish his job
                if (TimeBetweenCognitiveCycles > 0)
                {
                    Thread.Sleep(TimeBetweenCognitiveCycles);
                }

				//Leticia
				//Creature c = (Creature) currentSceneInWS3D.Where(item => (item.CategoryId == Thing.CATEGORY_CREATURE)).First();

				/*if(c.isLeafletCompleted()){
					Console.WriteLine("LEAFLET COMPLETED -- Simulation finishes");
					CurrentCognitiveCycle = MaxNumberOfCognitiveCycles;
					//Abort(true);//nao sei se precisa disso realmente
				}*/
			}
        }
        #endregion

    }
}

/*
- Se não houver joias no campo de visão sensorial da criatura, gire em sentido horario para buscar novas joias.
- Se houver joias no campo de visão, vá em direção à joia mais proxima pertencente ao Leaflet. Se não houver joias pertencentes ao leaflet, vá até a jóia mais próxima.
- Se uma jóia estiver a menos de 30 unidades de distância da criatura, pegue a joia.
- Se um alimento estiver a menos de 40 unidades de distância da criatura, coma o alimento. 
- Se o leaflet já está completo, então nao faça nada.*/