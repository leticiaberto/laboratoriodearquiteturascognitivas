--Contents--
This distribution contains the following files:

readme.txt 						        - This file
LIDA-framework-non-commerical-v1.0.pdf  - LIDA framework non-commercial license agreement
lida-framework-changelog-v1.2b.pdf      - Summary of major changes from the previous version
The-LIDA-Tutorial.pdf					- A short tutorial of the LIDA model and LIDA framework
lida-framework-v1.2b.jar  		        - LIDA framework jar file
lib/								    - Directory of jar files required by the LIDA framework including:
									    - Jung 2.0.1 jung.sourceforge.net/ for the GUI.
									    - Colt 1.2.0 acs.lbl.gov/software/colt/ for Jung and SDM.
									    - JFreeChart 1.0.13 jfree.org/jfreechart/ for the GUI

lida-framework-v1.2b-doc.zip 	        - Archive of framework's javadoc
lida-framework-v1.2b-src.zip 	        - Archive of framework's source files
LidaXMLSchema.xsd				        - Xml schema file for "agent" xml declaration files
LidaFactories.xsd				        - Xml schema file for "factoryData" xml definition file
example/								- A basic example agent that illustrates some aspects of the framework.

--Information--
For more information please visit: http://ccrg.cs.memphis.edu/framework.html
									
--Contact--
For general inquiries or for inquiries about commercial licenses please write <ccrg@cs.memphis.edu>
For bug reporting please write <ccrg.memphis@gmail.com>

--Acknowledgements--
Javier Snaider – LIDA Framework main designer, developer, and team leader
Ryan McCall – LIDA Framework co-developer and designer
CCRG Computational Group - developing and testing
Stan Franklin – CCRG director and the LIDA Model founder