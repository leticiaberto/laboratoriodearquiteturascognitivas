/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package alifeagent.modules;

import edu.memphis.ccrg.lida.actionselection.Action;
import edu.memphis.ccrg.lida.actionselection.ActionSelection;
import edu.memphis.ccrg.lida.actionselection.ActionSelectionListener;
import edu.memphis.ccrg.lida.actionselection.PreafferenceListener;
import edu.memphis.ccrg.lida.framework.FrameworkModuleImpl;
import edu.memphis.ccrg.lida.globalworkspace.BroadcastContent;
import edu.memphis.ccrg.lida.globalworkspace.BroadcastListener;

/**
 *
 * @author leticia
 */
public class AdvancedActionSelection extends FrameworkModuleImpl implements
		ActionSelection, BroadcastListener{

    @Override
    public void decayModule(long ticks) {
    }

    @Override
    public void addActionSelectionListener(ActionSelectionListener listener) {
    }

    @Override
    public void addPreafferenceListener(PreafferenceListener listener) {
    }
    
    

    @Override
    public Action selectAction() {
        Action a = null;
        return a;
    }

    @Override
    public void receiveBroadcast(BroadcastContent bc) {
    }

    @Override
    public void learn(BroadcastContent bc) {
    }
    
}
